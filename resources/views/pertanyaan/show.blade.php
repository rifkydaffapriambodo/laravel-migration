@extends('layouts.master')

@section('content')
<div class="ml-3">
    <h3>{{ $pertanyaan->judul}}</h3>
    <p>{{$pertanyaan->isi}}</p>
</div>
@endsection