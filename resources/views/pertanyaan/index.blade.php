@extends('layouts.master')

@section('content')
<div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Daftar Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
            @endif
            <a class="btn btn-primary mb-3" href="/pertanyaan/create">Ajukan Pertanyaan</a>
            <br>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">id</th>
                        <th>Judul</th>
                        <th>Isi</th>
                        <th>Detail</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <!--<th style="width: 40px">Action</th>-->
                    </tr>
                </thead>
                <tbody>
                    @foreach($pertanyaan as $key => $pertanyaan)
                    <tr>
                        <td>{{ $key + 1}}</td>
                        <td>{{$pertanyaan->judul}}</td>
                        <td>{{$pertanyaan->isi}}</td>
                        <td>
                            <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm">lihat detail</a>
                        </td>
                        <td>
                            <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-info btn-sm">edit</a>
                        </td>
                        <td>
                            <form action="pertanyaan/{{$pertanyaan->id}}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                        <!--<td><span class="badge bg-danger">55%</span></td>-->
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">«</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">»</a></li>
            </ul>
        </div>
    </div>
</div>
@endsection